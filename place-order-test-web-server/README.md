## Getting Started
* Install [Docker Desktop](https://hub.docker.com/editions/community/docker-ce-desktop-windows)
* **Clone** [The Forge](https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/placeorder/the-forge) project from GitLab
### Instructions:
* After cloning The Forge project, open the-forge directory.
* Open '**place-order-test-web-server**' folder inside the directory.x
* Open a command prompt at this directory.
* Type: docker build -t place-order-image:1.0 .  <= That period is needed at the end of the line
   * **Note**: Docker will begin pulling from the nginx library, this could take a few seconds to complete.
* Once the images has been successfully built you can then type: docker run -d -p 1000:80 place-order-image:1.0
* you can now view the web page by typing: http://localhost:1000 into your web browser!

## You should now stop and delete all containers and images.
### Instructions:
* Open cmd and type: docker ps
* You should see a list of containers. Type: docker stop `<the first two characters of a containers id>`  <= angle brackets not needed
   * **Note**: you will need to stop each container individually
* Type: docker rm `<the first two characters of the containers id>`
   * **Note**: the rm command deletes the containers, you will also need to delete each container individually.
* Once all containers are stopped and deleted, type: docker images
   * **Note**: you should now see a list of images that are running.
* Type: docker rmi `<the first two characters of the images id>` 
   * **Note**: the rmi command deletes images. If it says the image depends on another image try deleting the other image first. Delete each image.
* Type: docker images
   * **Note**: there should be no images running.
* Type: docker ps -a
   * **Note**: you should see no containers running.



